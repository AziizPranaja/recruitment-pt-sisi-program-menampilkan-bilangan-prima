package main

import (
	"fmt"
	"math"
)

func main() {
		var n int
		fmt.Print("Input: ")
		fmt.Scan(&n)

		primes := []int{2}
		num := 3
		for len(primes) < n {
			isPrime := true
			for i := 2; i <= int(math.Sqrt(float64(num))); i++ {
				if num%i == 0 {
					isPrime = false
					break
				}
			}
			if isPrime {
				primes = append(primes, num)
			}
			num += 2
		}

		fmt.Print("Hasil: ")
		for i, prime := range primes {
			fmt.Printf("%d", prime)
			if i != len(primes)-1 {
				fmt.Print(", ")
			}
		}
	
}